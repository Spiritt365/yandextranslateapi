package ru.testing.gateway;

import com.google.gson.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.testing.entities.Translate;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = ""; //your API key

    @SneakyThrows
    public Translate getTranslate(String text, String langFrom, String langTo) {

        Gson gson = new Gson();
        JSONObject requestParams = new JSONObject();
        JSONArray textArr = new JSONArray();

        textArr.put(text);

        requestParams.put("sourceLanguageCode", langFrom);
        requestParams.put("targetLanguageCode", langTo);
        requestParams.put("languageCode", langFrom);
        requestParams.put("texts", textArr);
        String joString = requestParams.toString();

        URL url = new URL(URL);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.setRequestProperty("Authorization", "Api-Key "+ TOKEN);
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        urlConnection.setRequestProperty("file", requestParams.toString());
        urlConnection.connect();

        DataOutputStream dataOutputStream = new DataOutputStream(urlConnection.getOutputStream());
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(dataOutputStream, StandardCharsets.UTF_8));
        bufferedWriter.write(joString);
        bufferedWriter.close();
        dataOutputStream.close();

        if (!urlConnection.getResponseMessage().equals("OK")) {return null;}

        InputStream inputStream = urlConnection.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        StringBuffer stringBuffer = new StringBuffer();
        String inputLine;
        while ((inputLine = bufferedReader.readLine()) != null) {
            stringBuffer.append(inputLine);
        }
        bufferedReader.close();

        String jsonString = stringBuffer.toString();
        JsonElement jsonElement = new JsonParser().parse(jsonString);

        return gson.fromJson(jsonElement, Translate.class);
    }
}