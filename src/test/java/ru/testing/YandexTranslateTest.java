package ru.testing;

import org.junit.jupiter.api.DisplayName;
import ru.testing.entities.Translate;
import ru.testing.gateway.YandexTranslateGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class YandexTranslateTest {
    private static final String TEXT = "Hello World!";
    private static final String TEST_TEXT = "Всем Привет!";
    private static final String LANGUAGE_FROM = "en";
    private static final String LANGUAGE_TO = "ru";

    @Test
    @DisplayName("Перевод фразы \"Hello World!\"")
    public void getTranslate(){
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        Translate translate = yandexTranslateGateway.getTranslate(TEXT, LANGUAGE_FROM, LANGUAGE_TO);
        Assertions.assertEquals(translate.getTranslations().get(0).getText(), TEST_TEXT);
    }
}